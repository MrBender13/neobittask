#include <sys/socket.h>
#include<stdio.h>
#include<sys/types.h>
#include<stdlib.h>
#include<unistd.h>
#include<netinet/in.h>
#include<netdb.h>
#include<string.h>

const int PORT_NUMBER = 80;
const char* HOST_NAME = "192.168.1.1";
const int BUFFER_SIZE = 256; 

int main() {
    //creating socket
    int mySocket = socket(AF_INET, SOCK_STREAM, 0);
    if(!mySocket) printf("Error creating socket!\n");
    
    //converting hostname to ip address
    struct hostent *server = gethostbyname(HOST_NAME);
    if(!server) printf("No such host\n");
    
    //configuring connection
    struct sockaddr_in serv_addr;
    bzero((char*) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT_NUMBER);
    bcopy((char*)server->h_addr, (char*)&serv_addr.sin_addr.s_addr,
           server->h_length);
    
    //connection to server
    if(connect(mySocket, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        printf("Connection to server failed!\n");
	return 0;
    } else {
        printf("Connected to server, please enter the command: \n");
    }
    
    //sending command and getting answer
    char buffer[BUFFER_SIZE];
    bzero(buffer, BUFFER_SIZE);
    fgets(buffer, BUFFER_SIZE, stdin);
    
    if(!write(mySocket, buffer,strlen(buffer))) 
        printf("Error sending command!\n");

    bzero(buffer, BUFFER_SIZE);
    if(!read(mySocket, buffer, BUFFER_SIZE))
        printf("Error getting answer!\n");
    printf("%s\n", buffer);

    close(mySocket); 
    return 0;
}





















