import socket
import threading
import datetime

class SocketThread(threading.Thread):
    def __init__(self, client_sock, client_addr):
	threading.Thread.__init__(self)
	self.sock = client_sock
	self.addr = client_addr

    def run(self):
        command = self.sock.recv(1024)
	
	if command[:-1] == "getinfo":
	    time = datetime.datetime.now()
	    host_name = socket.gethostname()
	    host_ip = str(socket.gethostbyname_ex(host_name)[2][0])#ip is third in result tuple
	    
	    answer = self.addr[0] + " " + host_ip + " " +\
		     host_name + " " + str(time.hour) + ":"+\
	             str(time.minute) + ":" + str(time.second) +\
		     "\n"
	else:
	    answer = "Unknown command\n"
	
	self.sock.sendall(answer)
	self.sock.close()	
	


hostname = "192.168.1.1"
port = 80
queue_size = 3

#creating socket
server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, proto=0)
server_sock.bind((hostname, port))
server_sock.listen(queue_size)

while True:
    #getting request
    client_sock, client_addr = server_sock.accept()
    #just to know that smth occurs
    print('Connected by', client_addr)
    #processing request
    SocketThread(client_sock, client_addr).start()

